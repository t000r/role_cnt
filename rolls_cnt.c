#define 	F_CPU   8000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h>
#include <avr/wdt.h>
#include <avr/pgmspace.h>
#include <avr/sleep.h>
#include <avr/eeprom.h>
#include <avr/delay.h>

#include "EEPROM.C"

void display (void);

#define DDR_MODULATION	DDRC
#define PORT_MODULATION	PORTC
#define PT_MODULATION	PC0

#define DDR_INPUT	DDRD
#define PIN_INPUT	PIND
#define PT1_INPUT	PD0
#define PT2_INPUT	PD1

volatile unsigned char to_counter = 0;
volatile unsigned int m_count = 0;
volatile unsigned int c1 = 0;
volatile unsigned int c2 = 0;

volatile unsigned char port_state;
volatile unsigned char port_state_last_time;
volatile unsigned int  sec = 0;


unsigned int rolls;
unsigned int rolls_all;

volatile unsigned char p0_flag = 0;


unsigned int disp_word;

#define devide_ms -250

void init_timer0(void) 
{
  TCCR0 = (_BV (CS01) );				/* TCCR0: �������� �� ������� CK / 8   */
  TCNT0 = devide_ms;   //
  TIMSK|=(_BV (TOIE0) );   			/* TOIE0: Timer/Counter0 Overflow Interrupt Enable */
  // 80���
}

unsigned char mass [] = {11,11,11,11};

SIGNAL (SIG_OVERFLOW0)         //���������� ���������� ��� ������������� ������� 0
{
TCNT0 = devide_ms;
sei(); 


m_count++;
if (!(m_count&0x03))
	PORTD &=~(1 << PD5);
else	
	PORTD |=(1 << PD5);

if (!(m_count&0x7FF))
	{
	if (sec)
		{
		sec++;
		//rolls++;
		}
	}

}


unsigned int ch1 [] = {0x7007, 0x4001, 0x300B, 0x600B, 0x400D, 0x600E, 0x700E, 0x4003, 0x700F, 0x600F, 0, 0x0008};
unsigned int ch2 [] = {0x0770, 0x0410, 0x03B0, 0x06B0, 0x04D0, 0x06E0, 0x07E0, 0x0430, 0x07F0, 0x06F0, 0, 0x0008};
unsigned int ch3 [] = {0x7007, 0x4001, 0x300B, 0x600B, 0x400D, 0x600E, 0x700E, 0x4003, 0x700F, 0x600F, 0, 0x0080};
unsigned int ch4 [] = {0x1A38, 0x1020, 0x0A68, 0x1868, 0x1070, 0x1858, 0x1A58, 0x1028, 0x1A78, 0x1878, 0, 0x0040};

void display (void)
{
unsigned char i;
PORTD &=~(1 << PD6);
PORTD &=~(1 << PD7);

for (i = 0; i<16; i++)
	{
	if ((ch1[mass[0]])&(1<<i))
		PORTB |= (1 << PB0);
	else
		PORTB &=~(1 << PB0);	
	
	
	if ((ch2[mass[1]]|ch3[mass[2]]  /*| ((mass[2]<10)?(0x8000):(0))*/  )&(1<<i))
		PORTB |= (1 << PB1);
	else
		PORTB &=~(1 << PB1);
	
	if ((ch4[mass[3]]    | ((p0_flag)? (1 << 10):(0))  )&(1<<i))
		PORTB |= (1 << PB2);
	else
		PORTB &=~(1 << PB2);	

	PORTD |= (1 << PD7);
	PORTD &=~(1 << PD7);	
	
	}
PORTD &=~(1 << PD6);
PORTD |= (1 << PD6);
PORTD &=~(1 << PD6);	

//PORTD &=~(1 << PD5);
}


#define BUTTON_RESET_DDR	DDRC
#define BUTTON_RESET_PULLUP	PORTC
#define BUTTON_RESET_PIN	PINC
#define BUTTON_RESET_PT		PC3

#define SENSOR_ROLL_DDR		DDRC
#define SENSOR_ROLL_PULLUP	PORTC
#define SENSOR_ROLL_PIN		PINC
#define SENSOR_ROLL_PT		PC0

#define EE_rolls		0x20
#define EE_rolls_all	0x40

int main(void)
{
init_timer0();
init_timer0();

DDRB |= (1<<PB0);
DDRB |= (1<<PB1);
DDRB |= (1<<PB2);

DDRD |= (1<<PD5);
DDRD |= (1<<PD6);
DDRD |= (1<<PD7);

BUTTON_RESET_DDR 	&= (1 << BUTTON_RESET_PT);
BUTTON_RESET_PULLUP |= (1 << BUTTON_RESET_PT);

SENSOR_ROLL_DDR 	&= (1 << SENSOR_ROLL_PT);
SENSOR_ROLL_PULLUP  |= (1 << SENSOR_ROLL_PT);


//clear_probeg (EE_rolls);
//clear_probeg (EE_rolls_all);

rolls 		= Read_probeg (EE_rolls);
rolls_all 	= Read_probeg (EE_rolls_all);

sei();

display(); // ----
// delay 
_delay_ms (1000);



mass [0] =  rolls_all / 1000;
mass [1] = (rolls_all / 100) % 10;
mass [2] = (rolls_all / 10) % 10;
mass [3] = rolls_all % 10;
display();
_delay_ms (1000);

mass [0] = mass [1] = mass [2] = mass [3] = 11; // ----
display();
_delay_ms (1000);
sec = 0;




while (1)
	{
	mass [0] =  rolls / 1000;
	mass [1] = (rolls / 100) % 10;
	mass [2] = (rolls / 10 ) % 10;
	mass [3] =  rolls % 10;
	display();
	
	// ������ ��������
	if ( (bit_is_clear (SENSOR_ROLL_PIN, BUTTON_RESET_PT)) && (sec == 0) )
		sec = 1; // �������� ������� �������
	
	
	if (bit_is_set (SENSOR_ROLL_PIN, BUTTON_RESET_PT))
		{
		if (sec > 3) // ���� ������������ ������� �����
			{
			rolls++; 			
			rolls_all++;		
			Write_probeg (EE_rolls, 		rolls);
			Write_probeg (EE_rolls_all, 	rolls_all);
			
			if (rolls > 9999)  		{rolls = 0; 		clear_probeg (EE_rolls);}
			if (rolls_all > 9999)  	{rolls_all = 0; 	clear_probeg (EE_rolls_all);}
			}
		sec = 0;	
		}
		
	
	// ��������� �����-�������
	if (sec > 3)
		p0_flag = 1;
	else
		p0_flag = 0;
	
	// ��������� ������
	static unsigned long button_delay;
	if (bit_is_clear (BUTTON_RESET_PIN, SENSOR_ROLL_PT))	
		{
		button_delay++;
		if (button_delay > 10000)
			{rolls = 0; 		clear_probeg (EE_rolls);  button_delay = 0;}
		}
	else
		button_delay = 0;
	}
}



